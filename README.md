# Einstieg ins Forschungsdatenmanagement mit git und GitLab

Datum: 21.09.2021, 12:00 - 17:00 Uhr

Format: Flipped Classroom in und mit GitLab, ~1,5h Vorbereitungszeit

## Folien

<a href="./slides">Vortragsfolien</a>
